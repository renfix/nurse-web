import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DataViewModule } from 'primeng/dataview';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { AvatarModule } from 'primeng/avatar';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { TagModule } from 'primeng/tag';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { DialogModule } from 'primeng/dialog';
import { SidebarModule } from 'primeng/sidebar';
import { MenuModule } from 'primeng/menu';
import {SharedModule} from '../../../shared/sharedModule'

import { PatientListRoutingModule } from './patient-list-routing.module';
import { PatientListComponent } from './patient-list.component';


@NgModule({
  declarations: [
    PatientListComponent
  ],
  imports: [
    CommonModule,SharedModule,FormsModule,ReactiveFormsModule,MenuModule,DialogModule,SidebarModule,ChipsModule,
    PatientListRoutingModule,ConfirmPopupModule,ConfirmDialogModule,ToastModule,TableModule,TagModule,MessagesModule,MessageModule,DropdownModule,ButtonModule,AvatarModule,DividerModule,CalendarModule
  ]
})
export class PatientListModule { }

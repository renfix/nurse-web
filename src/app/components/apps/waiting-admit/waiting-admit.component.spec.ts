import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingAdmitComponent } from './waiting-admit.component';

describe('WaitingAdmitComponent', () => {
  let component: WaitingAdmitComponent;
  let fixture: ComponentFixture<WaitingAdmitComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WaitingAdmitComponent]
    });
    fixture = TestBed.createComponent(WaitingAdmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

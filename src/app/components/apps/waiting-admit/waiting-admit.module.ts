import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WaitingAdmitRoutingModule } from './waiting-admit-routing.module';
import { WaitingAdmitComponent } from './waiting-admit.component';


@NgModule({
  declarations: [
    WaitingAdmitComponent
  ],
  imports: [
    CommonModule,
    WaitingAdmitRoutingModule
  ]
})
export class WaitingAdmitModule { }

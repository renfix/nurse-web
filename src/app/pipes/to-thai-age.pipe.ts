
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
    name: 'thaiage'
})
export class ThaiAgePipe implements PipeTransform {
  transform(age: string): string {
    //   console.log(age);
  

    let myArray = age.split("-");
    let tAge = parseInt(myArray[0])+" ปี "+parseInt(myArray[1])+" เดือน "+parseInt(myArray[2])+" วัน ";
   
    return tAge;
  }
}